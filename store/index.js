import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      pages: []
    },

    mutations: {
      setPages(stage, pages) {
        stage.pages = pages
      },
      addPage(stage, page) {
        stage.pages.push(page);
      },
      updatePage(stage, updatedPage) {
        const pageIndex = stage.pages.findIndex(page => page._id === updatedPage._id);
        state.pages[pageIndex] = updatedPage;
      },
      removePage(stage, deletedPage) {
        const pageIndex = stage.pages.findIndex(page => page._id === deletedPage._id);
        stage.pages.splice(pageIndex, 1);
      }
    },

    actions: {
      /**
       * @param {Object} vuexContext
       */
      nuxtServerInit(vuexContext) {
        return new Promise(async (resolve, reject) => {
          await this.$axios.$get('/pages')
            .then(response => {
              vuexContext.commit('setPages', response.data);
              resolve();
            })
            .catch(reject);
        });
      },

      /**
       * @param {Object} vuexContext
       * @param {Array} pages
       */
      setPages(vuexContext, pages) {
        vuexContext.commit('setPages', pages)
      },

      /**
       * @param {Object} vuexContext
       * @param {string} page
       */
      addPage(vuexContext, page) {
        return this.$axios.post('/pages', page)
          .then(response => {
            vuexContext.commit('addPage', response.data);

            return response.data;
          });
      },

      /**
       * @param {Object} vuexContext
       * @param {string} page
       */
      updatePage(vuexContext, page) {
        return this.$axios.put('/pages/' + page._id, page)
          .then(response => {
            vuexContext.commit('updatePage', response.data);
          });
      },

      /**
       * @param {Object} vuexContext
       * @param {string} page
       */
      removePage(vuexContext, page) {
        return this.$axios.delete('/pages/' + page._id)
          .then(response => {
            vuexContext.commit('removePage', page);
          });
      }
    },
    getters: {
      getPages(state) {
        return state.pages
      },
      getPage(state) {
        return function(id) {
          return state.pages.find(page => page._id === id);
        }
      }
    }
  })
}

export default createStore
