FROM node:12.16.0-alpine as develop-stage
WORKDIR /app
COPY package*.json ./
RUN yarn install
COPY . .

# build stage
FROM develop-stage as build-stage
RUN yarn generate

# production stage
FROM nginx:1.15.7-alpine as production-stage
COPY --from=build-stage /app/.nuxt /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
