import axios from "axios";

const request = axios.create({
  baseURL: process.env.API_URL,
  timeout: 5000
});


export function createPage(data: Object) {
  return request({
    url: "/pages",
    method: "post",
    data
  });
}

export function findPage(pageId: string) {
  return request({
    url: "/pages/" + pageId,
    method: "get"
  });
}

export function removePage(pageId: string) {
  return request({
    url: "/pages/" + pageId,
    method: "delete"
  });
}
